﻿using SilverMagicBytes;
foreach(var t in MagicByteCombos.GetAll())
{
    Console.WriteLine($"{t.MimeType} {string.Join(',', t.Combo.Select(x=>x?.ToString("x2")))}");
}
foreach (var file in Directory.EnumerateFiles("files"))
{
    FileStream s = File.OpenRead(file);
    var mt = MagicByteCombos.Match(s, 0)?.MimeType;
    Console.WriteLine(file+" "+mt);
}