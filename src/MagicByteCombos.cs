﻿using System.Text;
using System.Text.Unicode;

namespace SilverMagicBytes;

public static class MagicByteCombos
{
    static MagicByteCombos()
    {
        for (int a = 0; a < ByteCombos.Count; a++)
        {
            if (ByteCombos[a].MimeType is TextBasedMime && !ByteCombos[a].Combo.Contains(null) && ByteCombos[a].Combo.Cast<byte>().ToArray() is { } cast && Utf8.IsValid(cast))
            {
                var current = ByteCombos[a];
                var decoded = Encoding.UTF8.GetString(cast);
                var utf8bom = new UTF8Encoding(true);
                var preamble = utf8bom.GetPreamble();
                byte[] utf8b = new byte[preamble.Length + cast.Length];
                Array.Copy(preamble, 0, utf8b, 0, preamble.Length);
                Array.Copy(cast, 0, utf8b, preamble.Length, cast.Length);
                ByteCombos.Insert(++a, new MagicByteComboWithMimeType(current.MimeType, utf8b));
                for (int le = 0; le <= 1; le++)
                {
                    for (int bom = 0; bom <= 1; bom++)
                    {
                        var utf16 = new UnicodeEncoding(le == 1, bom == 1);
                        ByteCombos.Insert(++a, new MagicByteComboWithMimeType(current.MimeType, utf16.GetBytes(decoded.Replace("UTF-8",$"UTF-16{(le==1?"LE":"BE")}"))));
                        var utf32 = new UTF32Encoding(le == 1, bom == 1);
                        ByteCombos.Insert(++a, new MagicByteComboWithMimeType(current.MimeType, utf32.GetBytes(decoded.Replace("UTF-8",$"UTF-32{(le==1?"LE":"BE")}"))));
                    }
                }
            }
        }
    }
    private static List<MagicByteComboWithMimeType> ByteCombos =
    [
       new MagicByteComboWithMimeType(KnownMimes.MP3Mime, 0xFF, 0xFB),
        new MagicByteComboWithMimeType(KnownMimes.MP3Mime, 0xFF, 0xF3),
        new MagicByteComboWithMimeType(KnownMimes.MP3Mime, 0xFF, 0xF2),
        new MagicByteComboWithMimeType(KnownMimes.MP3Mime, 0x49, 0x44, 0x33),

        new MagicByteComboWithMimeType(KnownMimes.AACMime, 0xFF, 0xF1),
        new MagicByteComboWithMimeType(KnownMimes.AACMime, 0xFF, 0xF9),
        new MagicByteComboWithMimeType(KnownMimes.OGGMime, 0x4f, 0x67, 0x67, 0x53),

        new MagicByteComboWithMimeType(KnownMimes.MpegMime, 0x47),
        new MagicByteComboWithMimeType(KnownMimes.Mp2Mime, 0x00, 0x00, 0x01, 0xBA),
        new MagicByteComboWithMimeType(KnownMimes.Mp4Mime, 0x66, 0x74, 0x79, 0x70, 0x69, 0x73, 0x6F, 0x6D),

        new MagicByteComboWithMimeType(KnownMimes.FLACMime, "fLaC"u8.ToArray()),
        new MagicByteComboWithMimeType(KnownMimes.MidMime, 0x4D, 0x54, 0x68, 0x64),
        new MagicByteComboWithMimeType(KnownMimes.WAVMime, 0x52, 0x49, 0x46, 0x46, null, null, null, null, 0x57, 0x41,
            0x56, 0x45),
        new MagicByteComboWithMimeType(KnownMimes.AiffMime, 0x46, 0x4F, 0x52, 0x4D, null, null, null, null, 0x41, 0x49,
            0x46, 0x46),

        new MagicByteComboWithMimeType(KnownMimes.PngMime, 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A),
        new MagicByteComboWithMimeType(KnownMimes.JPGMime, 0xFF, 0xD8, 0xFF, 0xDB),
        new MagicByteComboWithMimeType(KnownMimes.JPGMime, 0xFF, 0xD8, 0xFF, 0xE0),
        new MagicByteComboWithMimeType(KnownMimes.JPGMime, 0xFF, 0xD8, 0xFF, 0xE0, 0x00, 0x10, 0x4A, 0x46, 0x49, 0x46,
            0x00, 0x01),
        new MagicByteComboWithMimeType(KnownMimes.JPGMime, 0xFF, 0xD8, 0xFF, 0xEE),
        new MagicByteComboWithMimeType(KnownMimes.JPGMime, 0xFF, 0xD8, 0xFF, 0xE1, null, null, 0x45, 0x78, 0x69, 0x66,
            0x00, 0x00),
        new MagicByteComboWithMimeType(KnownMimes.SVGMime, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<svg"u8.ToArray()),
        new MagicByteComboWithMimeType(KnownMimes.SVGMime, "<svg"u8.ToArray()),
        new MagicByteComboWithMimeType(KnownMimes.XMLMime, "<?xml"u8.ToArray()),
        new MagicByteComboWithMimeType(KnownMimes.SplMime,"Spl"u8.ToArray()),
        new MagicByteComboWithMimeType(KnownMimes.M3uMime,"#EXTM3U"u8.ToArray()),
        new MagicByteComboWithMimeType(KnownMimes.MKVMime, 0x1A, 0x45, 0xDF, 0xA3),
        new MagicByteComboWithMimeType(KnownMimes.Mod3TrackerMime, 0x4D, 0x4F ,0x33),
        new MagicByteComboWithMimeType(KnownMimes.ImpulseTrackerMime, "IMPM"u8.ToArray()),
        new MagicByteComboWithMimeType(KnownMimes.S3MTrackerMime,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,83, 67, 82, 77),
        new MagicByteComboWithMimeType(KnownMimes.ExtendedTrackerMime, null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,  0x1A),
        new MagicByteComboWithMimeType(KnownMimes.ModTrackerMime,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,77, 46, 75, 46),
        new MagicByteComboWithMimeType(KnownMimes.OctetMime)
    ];

    public static MagicByteComboWithMimeType? Match(Stream s, long offset)
    {
        return ByteCombos.Find(combo => combo.Match(s, offset));
    }

    public static bool MatchesAny(Stream s, long offset, string mimetype)
    {
        return ByteCombos.Any(x =>
            (x.MimeType.Common == mimetype || x.MimeType.AlternativeTypes.Contains(mimetype)) && x.Match(s, offset));
    }

    public static void AddMBC(MagicByteComboWithMimeType mbc)
    {
        var x = ByteCombos.FindIndex(y => y.MimeType == KnownMimes.OctetMime);
        if (x == -1)
            ByteCombos.Add(mbc);
        else
            ByteCombos.Insert(x, mbc);
    }

    public static void OverrideMBC(MagicByteComboWithMimeType mbc)
    {
        foreach (var bc in ByteCombos.Where(x => x.MimeType == mbc.MimeType)) ByteCombos.Remove(bc);
        ByteCombos.Add(mbc);
    }
    public static void OverrideMBC(MagicByteComboWithMimeType[] mbc)
    {
        foreach (var bc in ByteCombos.Where(x => mbc.Any(y => x.MimeType == y.MimeType))) ByteCombos.Remove(bc);
        ByteCombos.AddRange(mbc);
    }
    public static void RemoveMBC(MagicByteComboWithMimeType mbc)
    {
        ByteCombos.Remove(mbc);
    }

    public static MagicByteComboWithMimeType[] GetAll() => [.. ByteCombos];
}