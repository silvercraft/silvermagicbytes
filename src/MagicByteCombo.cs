﻿using System.Diagnostics;

namespace SilverMagicBytes;

public class MagicByteCombo
{
    public readonly byte?[] Combo;

    public MagicByteCombo(params byte?[] combo)
    {
        Combo = combo;
    }
    public MagicByteCombo(byte[] combo)
    {
        Combo = combo.Cast<byte?>().ToArray();
    }
    public bool Match(Stream s, long offset)
    {
        ArgumentNullException.ThrowIfNull(s);
        if (!s.CanRead)
            throw new NotSupportedException($"The stream {nameof(s)} does not support reading? ");
        if (offset != 0 && !s.CanSeek)
            Debug.WriteLine($"SilverMagicBytes.MagicByteCombo.Match: Cannot seek unseekable stream :(");
        var comboOffset = 0;
        if (s.CanSeek)
        {
            try
            {
                if (s.Length < offset)
                {
                    return false;
                }
            }
            catch (NotSupportedException)
            {
                //Worth a try
            }
            s.Position = offset;
            if (Combo.Length > 10 && Combo[0] is null && Combo[10] is null)
            {
                comboOffset = Array.FindIndex(Combo, x => x is not null) - 1;
                s.Position += comboOffset;
                Debug.WriteLine($"SilverMagicBytes.MagicByteCombo.Match: Skipping {comboOffset} null checks by seeking");
            }
            try
            {
                if (s.Length < offset)
                {
                    return false;
                }
            }
            catch (NotSupportedException)
            {
                //Worth a try
            }
        }
        for (var o = comboOffset; o < Combo.Length; o++)
        {
            if (Combo[o] is { } b)
            {
                if (s.ReadByte() != b) return false;
            }
            else
            {
                if (s.ReadByte() == -1)
                {
                    return false;
                }
            }
        }
        return true;
    }
}